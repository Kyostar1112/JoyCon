﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Example : MonoBehaviour
{
    private static readonly Joycon.Button[] m_buttons =
        Enum.GetValues(typeof(Joycon.Button)) as Joycon.Button[];

    private List<Joycon> m_joycons;
    private Joycon m_joycon1L;
    private Joycon m_joycon1R;
    private Joycon m_joycon2L;
    private Joycon m_joycon2R;
    private Joycon.Button? m_pressedButton1L;
    private Joycon.Button? m_pressedButton1R;
    private Joycon.Button? m_pressedButton2L;
    private Joycon.Button? m_pressedButton2R;

    private void Start()
    {
        m_joycons = JoyconManager.Instance.j;
        if (m_joycons == null || m_joycons.Count <= 0) return;
        m_joycon1L = m_joycons.Find(c => c.isLeft);
        m_joycon1R = m_joycons.Find(c => !c.isLeft);
        m_joycon2L = m_joycons.Find(c => c.isLeft);
        m_joycon2R = m_joycons.Find(c => !c.isLeft);
    }

    private void Update()
    {
        m_pressedButton1L = null;
        m_pressedButton1R = null;
        m_pressedButton2L = null;
        m_pressedButton2R = null;
        if (m_joycons == null || m_joycons.Count <= 0) return;
        foreach (var button in m_buttons)
        {
            if (m_joycon1L.GetButton(button))
            {
                m_pressedButton1L = button;
            }
            if (m_joycon1R.GetButton(button))
            {
                m_pressedButton1R = button;
            }
            if (m_joycon2L.GetButton(button))
            {
                m_pressedButton2L = button;
            }
            if (m_joycon2R.GetButton(button))
            {
                m_pressedButton2R = button;
            }
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            m_joycon2L.SetRumble(160, 320, 0.6f, 200);
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            m_joycon1R.SetRumble(160, 320, 0.6f, 200);
        }
    }

    private void OnGUI()
    {
        var style = GUI.skin.GetStyle("label");
        style.fontSize = 24;
        if (m_joycons == null || m_joycons.Count <= 0)
        {
            GUILayout.Label("Joy-Con が接続されていません");
            return;
        }
        if (!m_joycons.Any(c => c.isLeft))
        {
            GUILayout.Label("Joy-Con (L) が接続されていません");
            return;
        }
        if (!m_joycons.Any(c => !c.isLeft))
        {
            GUILayout.Label("Joy-Con (R) が接続されていません");
            return;
        }
        GUILayout.BeginHorizontal(GUILayout.Width(960));
        foreach (var joycon in m_joycons)
        {
            var isLeft = joycon.isLeft;
            var name = isLeft ? "Joy-Con (L)" : "Joy-Con (R)";
            var key = isLeft ? "Z キー" : "X キー";
            var button = isLeft ? m_pressedButton1L : m_pressedButton1R;
            var stick = joycon.GetStick();
            var gyro = joycon.GetGyro();
            var accel = joycon.GetAccel();
            var orientation = joycon.GetVector();
            GUILayout.BeginVertical(GUILayout.Width(0));
            GUILayout.Label(name);
            GUILayout.Label(key + "：振動");
            GUILayout.Label("押されているボタン：" + button);
            GUILayout.Label(string.Format("スティック：({0}, {1})", stick[0], stick[1]));
            GUILayout.Label("ジャイロ：" + gyro);
            GUILayout.Label("加速度：" + accel);
            GUILayout.Label("傾き：" + orientation);
            GUILayout.EndVertical();
        }
        GUILayout.EndHorizontal();
    }
}